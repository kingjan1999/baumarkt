// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('baumarkt', ['ionic', 'baumarkt.controllers', 'ngCordova']);

app.run(function ($ionicPlatform, $cordovaStatusbar) {

    if (typeof($cordovaStatusbr) !== 'undefined') {
        console.log('Setze');
        $cordovaStatusbar.overlaysWebView(true);
        $cordovaStatusBar.style(1); //Light
    }
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
        ionic.Platform.fullScreen();
    });
});

app.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('app', {
            url: "/app",
            abstract: true,
            templateUrl: "templates/menu.html",
            controller: 'AppCtrl'
        })

        .state('app.search', {
            url: "/search",
            views: {
                'menuContent': {
                    templateUrl: "templates/search.html",
                    controller: 'SearchCtrl'
                }
            }
        })

        /* .state('app.results', {
         url : '/search/results',
         views: {
         'menuContent': {
         templateUrl: "templates/products.html",
         controller: 'SearchCtrl'
         }
         }
         })*/

        .state('app.start', {
            url: "/start",
            views: {
                'menuContent': {
                    templateUrl: "templates/start.html",
                    controller: "StartCtrl"
                }
            }
        })
        .state('app.map', {
            url: "/map",
            views: {
                'menuContent': {
                    templateUrl: "templates/map.html",
                    controller: "MapCtrl"
                }
            }
        })
        .state('app.products', {
            url: "/products",
            views: {
                'menuContent': {
                    templateUrl: "templates/products.html",
                    controller: 'ProductsCtrl'
                }
            }
        })
        .state('app.cat', {
            url: "/products/:catId",
            views: {
                'menuContent': {
                    templateUrl: "templates/products.html",
                    controller: 'CatsCtrl'
                }
            }
        })
        .state('app.product', {
            url: "/product/:productId",
            views: {
                'menuContent': {
                    templateUrl: "templates/product.html",
                    controller: 'ProductCtrl'
                }
            }
        })
        .state('app.about', {
            url: '/about',
            views: {
                'menuContent': {
                    templateUrl: "templates/about.html"
                }
            }
        }).state('app.todos', {
            url: "/todos",
            views: {
                'menuContent': {
                    templateUrl: "templates/todos.html",
                    controller: "TodoCtrl"
                }
            }
        })
    ;

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/start');
});

