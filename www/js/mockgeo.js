/**
 * Created by jan on 01.06.15.
 */
var map_data = {
    "type": "FeatureCollection",
    "features": [
    {
        "type": "Feature",
        "properties": {
            "name": ""
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [
                        9.56873506307602,
                        52.2115775862785
                    ],
                    [
                        9.568772614002228,
                        52.21146582701333
                    ],
                    [
                        9.56909716129303,
                        52.21150691501114
                    ],
                    [
                        9.569011330604553,
                        52.21160881308174
                    ],
                    [
                        9.56873506307602,
                        52.2115775862785
                    ]
                ]
            ]
        }
    },
    {
        "type": "Feature",
        "properties": {
            "name": "Eingang",
            "marker-color": "#b02929",
            "marker-size": "medium",
            "marker-symbol": ""
        },
        "geometry": {
            "type": "Point",
            "coordinates": [
                9.568909406661987,
                52.21154964648856
            ]
        }
    },
    {
        "type": "Feature",
        "properties": {
            "name": ""
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [
                        9.568775296211243,
                        52.21145432236714
                    ],
                    [
                        9.568515121936798,
                        52.21141652136577
                    ],
                    [
                        9.568563401699066,
                        52.21131133580146
                    ],
                    [
                        9.568818211555481,
                        52.21134913689232
                    ],
                    [
                        9.568775296211243,
                        52.21144610476088
                    ],
                    [
                        9.568775296211243,
                        52.21145432236714
                    ]
                ]
            ]
        }
    },
    {
        "type": "Feature",
        "properties": {
            "marker-color": "#7e7e7e",
            "marker-size": "medium",
            "marker-symbol": "",
            "name": "Schrauben"
        },
        "geometry": {
            "type": "Point",
            "coordinates": [
                9.568689465522766,
                52.211395155568184
            ]
        }
    },
    {
        "type": "Feature",
        "properties": {
            "name": ""
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [
                        9.568777978420258,
                        52.21145267884599
                    ],
                    [
                        9.569094479084015,
                        52.21150198445342
                    ],
                    [
                        9.569153487682343,
                        52.21138200737993
                    ],
                    [
                        9.56883430480957,
                        52.21134913689232
                    ],
                    [
                        9.568777978420258,
                        52.21145267884599
                    ]
                ]
            ]
        }
    },
    {
        "type": "Feature",
        "properties": {
            "name": ""
        },
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [
                        9.568568766117096,
                        52.21130640522201
                    ],
                    [
                        9.568611681461334,
                        52.211183140558205
                    ],
                    [
                        9.568858444690704,
                        52.211216011168666
                    ],
                    [
                        9.568818211555481,
                        52.21133434516493
                    ],
                    [
                        9.568568766117096,
                        52.21130640522201
                    ]
                ]
            ]
        }
    },
    {
        "type": "Feature",
        "properties": {
            "marker-color": "#7e7e7e",
            "marker-size": "medium",
            "marker-symbol": "",
            "name": "Werkzeuge"
        },
        "geometry": {
            "type": "Point",
            "coordinates": [
                9.568684101104736,
                52.21128996995327
            ]
        }
    },
    {
        "type": "Feature",
        "properties": {
            "marker-color": "#7e7e7e",
            "marker-size": "medium",
            "marker-symbol": "",
            "name": "Dekoration"
        },
        "geometry": {
            "type": "Point",
            "coordinates": [
                9.56895500421524,
                52.211418164888244
            ]
        }
    },
    {
        "type": "Feature",
        "properties": {},
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [
                        9.569520950317381,
                        52.211058232016676
                    ],
                    [
                        9.568670690059662,
                        52.210946471444956
                    ],
                    [
                        9.56860363483429,
                        52.21112068633135
                    ],
                    [
                        9.569435119628906,
                        52.21122587234706
                    ],
                    [
                        9.569520950317381,
                        52.211058232016676
                    ]
                ]
            ]
        }
    },
    {
        "type": "Feature",
        "properties": {
            "marker-color": "#7e7e7e",
            "marker-size": "medium",
            "marker-symbol": "",
            "name": "Gartenbereich"
        },
        "geometry": {
            "type": "Point",
            "coordinates": [
                9.56907033920288,
                52.211127260464615
            ]
        }
    },
    {
        "type": "Feature",
        "properties": {},
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [
                        9.568836987018585,
                        52.2113409192666
                    ],
                    [
                        9.569174945354462,
                        52.21137050271204
                    ],
                    [
                        9.569215178489685,
                        52.211260386454214
                    ],
                    [
                        9.568877220153809,
                        52.21121929822839
                    ],
                    [
                        9.568836987018585,
                        52.2113409192666
                    ]
                ]
            ]
        }
    },
    {
        "type": "Feature",
        "properties": {
            "marker-color": "#7e7e7e",
            "marker-size": "medium",
            "marker-symbol": "",
            "name": "Bodenbeläge"
        },
        "geometry": {
            "type": "Point",
            "coordinates": [
                9.569003283977509,
                52.2113228404847
            ]
        }
    },
    {
        "type": "Feature",
        "properties": {},
        "geometry": {
            "type": "Polygon",
            "coordinates": [
                [
                    [
                        9.56922322511673,
                        52.21126860409482
                    ],
                    [
                        9.569440484046936,
                        52.21128668289881
                    ],
                    [
                        9.56932246685028,
                        52.21165483148863
                    ],
                    [
                        9.569024741649626,
                        52.21161374362763
                    ],
                    [
                        9.56922322511673,
                        52.21126860409482
                    ]
                ]
            ]
        }
    },
    {
        "type": "Feature",
        "properties": {
            "marker-color": "#7e7e7e",
            "marker-size": "medium",
            "marker-symbol": "",
            "name": "Lager"
        },
        "geometry": {
            "type": "Point",
            "coordinates": [
                9.569236636161804,
                52.2114378871531
            ]
        }
    }
]
}