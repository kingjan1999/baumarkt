/**
 * Created by jan on 02.06.15.
 */
angular.module('baumarkt.controllers').controller('ProductsCtrl', function ($scope, $cordovaDialogs) {
    $scope.prodLink = '#/app/products';
    $scope.products = cats;
    $scope.showprice = false;

    $scope.addToCart = function (prod) {
        liste.push({name: prod.title, pid: prod.id});
        if(typeof($cordovaDialogs) !== 'undefined') {
            $cordovaDialogs.alert('Das Produkt wurde auf die Einkausliste gesetzt', 'Aktion erfolgreich', 'OK');
            $cordovaDialogs.beep(1);
        } else {
            alert("Produkt hinzugefügt!");
        }
    };
})
    .controller('CatsCtrl', function ($scope, $stateParams) {
        console.log($stateParams.catId);
        $scope.prodLink = '#/app/product';
        $scope.products = getByCat($stateParams.catId);
        $scope.showprice = true;
    })
    .controller('ProductCtrl', function ($scope, $stateParams, $cordovaDialogs) {
        $scope.found = products.length > $stateParams.productId;
        $scope.product =  $scope.found ? products[$stateParams.productId] : {title: 'Nicht gefunden!'};

        $scope.addToCart = function (prod) {
            liste.push({name: prod.title, pid: prod.id});
            if(typeof($cordovaDialogs) !== 'undefined') {
                $cordovaDialogs.alert('Das Produkt wurde auf die Einkausliste gesetzt', 'Aktion erfolgreich', 'OK');
                $cordovaDialogs.beep(1);
            } else {
                alert("Produkt hinzugefügt!");
            }
        };
    })
    .controller('SearchCtrl', function ($scope) {
        $scope.search = {};
        $scope.products = products;
        $scope.doSearch = function () {
            var term = $scope.search.term;
            var found = [];
            products.forEach(function (value, ind) {
                var regex = new RegExp('?*' + term + '?*');
                if (value.title.match(regex)) {
                    found.push(value);
                }
            });
            console.log(found);
            $scope.products = found;
        }
    });

