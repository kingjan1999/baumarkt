/**
 * Created by jan on 04.06.15.
 */
angular.module('baumarkt.controllers').controller('TodoCtrl', function ($scope, $cordovaDialogs, $ionicPopup) {
    $scope.todos = liste;
    $scope.neu = '';

    $scope.removeItem = function (index) {
        liste.splice(index, 1);
    };

    $scope.moveItem = function (item, fromIndex, toIndex) {
        $scope.todos.splice(fromIndex, 1);
        $scope.todos.splice(toIndex, 0, item);
    };

    $scope.insert = function () {
        if (typeof($cordovaDialogs) !== 'undefined') {
            $cordovaDialogs.prompt('Produkt eingeben', 'Neues Produkt hinzufügen', ['Hinzufügen', 'Abbrechen']).then(function (result) {
                var input = result.input1;
                var btnIndex = result.buttonIndex;
                console.log(btnIndex);
                console.log(input);
                if (btnIndex === 1 && input !== "") {
                    liste.push({name: input, pid: -1});
                    $cordovaDialogs.alert('Produkt wurde hinzugefügt', 'Aktion erfolgreich');
                }
            });
        } else {
            $ionicPopup.prompt({
                title: 'Neues Produkt hinzufügen',
                template: 'Produkt eingeben',
                inputPlaceholder: 'Produkt',
                cancelText: 'Abbrechen'
            }).then(function (res) {
                console.log(res);
                if (res !== "") {
                    liste.push({name: res, pid: -1});
                    $ionicPopup.alert({template: 'Produkt wurde hinzugefügt', title: 'Aktion erfolgreich'});
                }
            });
        }
    }
});